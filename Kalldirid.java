import java.util.Scanner;

public class Kalldirid {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;
  private static boolean emperorSpokenTo = false;
  private static boolean lupineKnightSlain = false;
  private static boolean leonineKnightSlain = false;
  private static boolean hasLetter = false;

  public static void kalldirid() {
    System.out.println("\n\t1. Look around\n\t2. Visit the forge\n\t3. Visit the wizardry\n\t4. Visit the alchemy\n\t5. Visit the inn\n\t6. Visit the palace\n\t7. Leave Kalldirid");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou look around the Empire of Kalldirid. Tens of thousands of people, ranging from the common folk to elves to knights to mages, are wandering about the streets. The marketplace is diverse, offering a forge, a wizardry, and an alchemy. A colossal palace serves as the primary sight of the city.");
      Kalldirid.kalldirid();
    }
    else if(choice == 2) {
      Shop.kalldiridForge();
    }
    else if(choice == 3) {
      Shop.kalldiridWizardry();
    }
    else if(choice == 4) {
      Shop.kalldiridAlchemy();
    }
    else if(choice == 5) {
      System.out.println("\nYou visit the Golden Lion, the most famous inn located within Kalldirid.");
      Kalldirid.inn();
    }
    else if(choice == 6) {
      System.out.println("\nYou visit the spectacular palace on the outskirts of Kalldirid.");
      Kalldirid.palace();
    }
    else if(choice == 7) {
      System.out.println("\nYou leave the Imperial City of Kalldirid.");
      Map2.move();
    }
    else {
      Kalldirid.kalldirid();
    }
  }

  public static void inn() {
    System.out.println("\n\t1. Talk to the innkeeper\n\t2. Check status\n\t3. Rest - 7 Gold\n\t4. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou talk to a man who appear");
      Kalldirid.inn();
    }
    else if(choice == 2) {
      Character.status();
      Kalldirid.inn();
    }
    else if(choice == 3) {
      if(Shop.getGold() >= 7) {
        Shop.changeGold(-7);
        Character.heal();
        System.out.println("\nYou rent a room and rest there for a while, restoring your life.");
      }
      else {
        System.out.println("\nNot enough gold!");
      }
      Kalldirid.inn();
    }
    else if(choice == 4) {
      System.out.println("\nYou exit the Golden Lion.");
      Kalldirid.kalldirid();
    }
    else {
      Kalldirid.inn();
    }
  }

  public static void palace() {
    System.out.println("\n\t1. Look around\n\t2. Talk to the Emperor\n\t3. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou take a tour around Kalldirid's palace. The palace functions as both a home for the Emperor and a meeting place for royals and officials. Everyone that you see is wearing lavish attire, with the exception of a few soldiers coated in armor.");
      Kalldirid.palace();
    }
    else if(choice == 2) {
      if(lupineKnightSlain && leonineKnightSlain && !hasLetter && emperorSpokenTo) {
        System.out.println("\n\"At long last, those malevolent knights have been stopped for good. Take this Royal Pendant as a token of gratitude. However, I have another request of you. Please listen to me, " + Character.getName() + ", for your reward will be greater than anything you can fathom. In the lost land of Zariadon, there is a great evil on the rise. The few soldiers that I have sent there to investigate have never returned. If you are willing, take this letter to the guards at Hanawyn and they will lead you to Zariadon.\"");
        System.out.println("\nYou received the Royal Pendant! All of you stats have increased.");
        Character.changeMaxLife(2);
        Character.changeMinDamage(1);
        Character.changeMaxDamage(1);
        Character.changeMagic(1);
        Character.changeSpeed(1);
        System.out.println("\nYou received the Emperor's Letter!");
        hasLetter = true;
        Kalldirid.palace();
      }
      else if(lupineKnightSlain && leonineKnightSlain && hasLetter && emperorSpokenTo) {
        System.out.println("\n\"Investigate the evil presence within Zariadon. I know that you are brave enough to face it, " + Character.getName() + ".\"");
        Kalldirid.palace();
      }
      else if(emperorSpokenTo) {
        System.out.println("\n\"Please, " + Character.getName() + ", go rid this land of those malicious knights.\"");
        Kalldirid.palace();
      }
      else {
        System.out.println("\nYou enter the royal chamber and speak with Emperor Kalahan, ruler of this land.");
        if(Character.getRole() == 1) {
          System.out.print("\n\n\"Knight ");
        }
        else if(Character.getRole() == 2) {
          System.out.print("\n\"Adventurer ");
        }
        else if(Character.getRole() == 3) {
          System.out.print("\n\"Rogue ");
        }
        System.out.println(Character.getName() + ", I have heard rumors of you. And I, Emperor Kalahan, have a request of you. Fret not, for you will receive a great reward. Two shady knights - one with the visage of a wolf, the other with the visage of a lion - have struck fear into the hearts of the civilians of this land. One can be found south of Kalldirid, in the forests of Gladehollow. The other can be found northwest of Kalldirid, in a ruined tower of yore. I hope this is enough information for you to track them down and slay them. Best of luck, " + Character.getName() + ".\"");
        emperorSpokenTo = true;
        Kalldirid.palace();
      }
    }
    else if(choice == 3) {
      System.out.println("\nYou exit the palace.");
      Kalldirid.kalldirid();
    }
    else {
      Kalldirid.palace();
    }
  }

  public static void slayLupineKnight() {
    lupineKnightSlain = true;
  }

  public static void slayLeonineKnight() {
    leonineKnightSlain = true;
  }

  public static boolean lupineKnightSlain() {
    return lupineKnightSlain;
  }

  public static boolean leonineKnightSlain() {
    return leonineKnightSlain;
  }

  public static boolean hasLetter() {
    return hasLetter;
  }
}