import java.util.Scanner;

public class Hanawyn {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;

  public static void hanawyn() {
    System.out.println("\n\t1. Look around\n\t2. Visit the armory\n\t3. Do some training\n\t4. Ask for an escort\n\t5. Leave Hanawyn");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou take a look around the military base of Hanawyn. It is surrounded by ramparts on all sides, and hundreds of soldiers can be seen training. There is a small residential area in the middle of the fortress.");
      Hanawyn.hanawyn();
    }
    else if(choice == 2) {
      Shop.hanawynArmory();
    }
    else if(choice == 3) {
      System.out.println("\nYou go to the training grounds.");
      Battle.soldierTraining();
      Hanawyn.hanawyn();
    }
    else if(choice == 4) {
      Hanawyn.escort();
    }
    else if(choice == 5) {
      System.out.println("\nYou leave the military camp of Hanawyn.");
      Map2.move();
    }
    else {
      Hanawyn.hanawyn();
    }
  }

  public static void escort() {
    if(Kalldirid.hasLetter()) {
      System.out.println("\nYou approach a soldier and inquire him about escorting you to Zariadon. You show him the Emperor's Letter, and he nods his head in understanding.");
      System.out.println("\n\"Are you sure you want to go to Zariadon? There's no turning back once we get there.\"");
      System.out.println("\n\t1. Yes\n\t2. No");
      choice = scan.nextInt();
      if(choice == 1) {
        System.out.println("\nYou decide to travel to Zariadon, escorted by the soldiers of Hanawyn. When you arrive they say their farewells and wish you luck as they leave.");
        Map3.move();
      }
      else if(choice == 2) {
        System.out.println("\nYou decide you need time to prepare and return to the entrance of Hanawyn.");
        Hanawyn.hanawyn();
      }
      else {
        Hanawyn.escort();
      }
    }
    else {
      System.out.println("\nYou don't have anywhere to travel to.");
      Hanawyn.hanawyn();
    }
  }
}