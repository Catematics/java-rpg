import java.util.Scanner;

public class Fazoria {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;
  private static boolean kingSpokenTo = false;
  private static boolean rewardReceived = false;

  public static void fazoria() {
    System.out.println("\n\t1. Look around\n\t2. Visit the marketplace\n\t3. Visit the inn\n\t4. Visit the palace\n\t5. Leave Fazoria");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nThe Northern Kingdom of Fazoria is massive. Hundreds of houses line the streets, and a grand palace lies in the center of everything. Crowded marketplaces can be found all around the city.");
      Fazoria.fazoria();
    }
    else if(choice == 2) {
      System.out.println("\nYou walk down to the bustling marketplace.");
      Shop.fazoriaShop();
    }
    else if(choice == 3) {
      System.out.println("\nYou visit the Silver Cat, one of the many inns located within Fazoria.");
      Fazoria.inn();
    }
    else if(choice == 4) {
      System.out.println("\nYou visit the colossal Fazoria Palace located at the heart of the city.");
      Fazoria.palace();
    }
    else if(choice == 5) {
      System.out.println("\nYou leave the Northern Kingdom of Fazoria");
      Map.move();
    }
    else {
      Fazoria.fazoria();
    }
  }

  public static void inn() {
    System.out.println("\n\t1. Talk to the innkeeper\n\t2. Check status\n\t3. Rest - 5 Gold\n\t4. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou talk to a dwarf, who you believe to be the innkeeper. He greets you warmly and says that you are welcome to come back any time.");
      Fazoria.inn();
    }
    else if(choice == 2) {
      Character.status();
      Fazoria.inn();
    }
    else if(choice == 3) {
      if(Shop.getGold() >= 5) {
        Shop.changeGold(-5);
        Character.heal();
        System.out.println("\nYou rent a room and rest there for a while, restoring your life.");
      }
      else {
        System.out.println("\nNot enough gold!");
      }
      Fazoria.inn();
    }
    else if(choice == 4) {
      System.out.println("\nYou exit the Silver Cat.");
      Fazoria.fazoria();
    }
    else {
      Fazoria.inn();
    }
  }

  public static void palace() {
    System.out.println("\n\t1. Explore the palace\n\t2. Talk to the King\n\t3. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou take a look around the palace. You see red carpets, marble pillars, and intricate paintings on the walls. Everywhere you go, guards and visitors are rushing around.");
      Fazoria.palace();
    }
    else if(choice == 2) {
      if(VarDormin.dragonSlain() && !rewardReceived && kingSpokenTo) {
        System.out.println("\n\"Could it be that the Dragon terrorizing Var Dormin has been slain? You have done well, " + Character.getName() + "! I shall reward you with gold and glory! You should use this gold to purchase a boat in the city of Yandelport, which is southeast of Fazoria.\"");
        Shop.changeGold(5000);
        System.out.println("\nYou gained 5000 gold!");
        rewardReceived = true;
        Fazoria.palace();
      }
      else if(VarDormin.dragonSlain() && rewardReceived && kingSpokenTo) {
        System.out.println("\n\"Have you considered visiting Yandelport yet? It's south of Var Dormin, near the sea.\"");
        Fazoria.palace();
      }
      else if(kingSpokenTo) {
        System.out.println("\n\"Have you slain the Dragon yet? Var Dormin can be found east of Fazoria.\"");
        Fazoria.palace();
      }
      else {
        System.out.println("\nAlas, you meet King Valik. You greet each other, and then he gives you a serious look and orders you to sit and listen to him.");
        if(Character.getRole() == 1) {
          System.out.print("\n\"Noble " + Character.getName() + ", I have a request of you.");
        }
        else if(Character.getRole() == 2) {
          System.out.print("\n\"Brave " + Character.getName() + ", I know you are just an adventurer, but I hope you hear my plea.");
        }
        else if(Character.getRole() == 3) {
          System.out.print("\n\"Mysterious " + Character.getName() + ", I normally do not resort to hiring rogues like you, but this is urgent.");
        }
        System.out.print(" A vile Dragon has wrought havoc on the nearby town of Var Dormin. They are an important ally of Fazoria. If you manage to defeat this Dragon, I will give you a great reward.\"\n");
        kingSpokenTo = true;
        Fazoria.palace();
      }
    }
    else if(choice == 3) {
      System.out.println("\nYou exit the palace.");
      Fazoria.fazoria();
    }
    else {
      Fazoria.palace();
    }
  }
}