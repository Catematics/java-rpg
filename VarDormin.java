import java.util.Scanner;

public class VarDormin {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;
  private static boolean dragonSlain = false;

  public static void varDormin() {
    if(!dragonSlain) {
      System.out.println("\nYou hear the distant roar of a Dragon and the screams of civilians.");
    }
    System.out.println("\n\t1. Look around\n\t2. Visit the shop\n\t3. Visit the inn\n\t4. Leave Var Dormin");
    choice = scan.nextInt();
    if(choice == 1) {
      if(!dragonSlain) {
        VarDormin.dragon();
      }
      else {
        System.out.println("\nYou look around Var Dormin. The town is still partially damaged, but it seems to be thriving regardless. As you walk around, some of the citizens thank you for slaying the Dragon.");
        VarDormin.varDormin();
      } 
    }
    else if(choice == 2) {
      if(!dragonSlain) {
        System.out.println("\nThe shop is closed.");
        VarDormin.varDormin();
      }
      else {
        System.out.println("\nYou go to Var Dormin's small but busy shop.");
        Shop.varDorminShop();
      }
    }
    else if(choice == 3) {
      if(!dragonSlain) {
        System.out.println("\nThe inn is closed.");
        VarDormin.varDormin();
      }
      else {
        System.out.println("\nYou visit the Green Fox, Var Dormin's one and only inn.");
        VarDormin.inn();
      }
    }
    else if(choice == 4) {
      System.out.println("\nYou leave Var Dormin.");
      Map.move();
    }
    else {
      VarDormin.varDormin();
    }
  }

  public static void dragon() {
    System.out.println("\nYou take a look around. Some houses are damaged, while others are broken to bits. As you approach the center of the town, you see a Crimson Dragon ravenously roaming around.");
    System.out.println("\n\t1. Fight the Dragon\n\t2. Run away");
    choice = scan.nextInt();
    if(choice == 1) {
      Battle.dragonBattle();
    }
    else if(choice == 2) {
      VarDormin.varDormin();
    }
    else {
      VarDormin.dragon();
    }
  }

  public static void inn() {
    System.out.println("\n\t1. Check status\n\t2. Rest - 5 Gold\n\t3. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      Character.status();
      VarDormin.inn();
    }
    else if(choice == 2) {
      if(Shop.getGold() >= 5) {
        Shop.changeGold(-5);
        Character.heal();
        System.out.println("\nYou rent a room and rest there for a while, restoring your life.");
      }
      else {
        System.out.println("\nNot enough gold!");
      }
      VarDormin.inn();
    }
    else if(choice == 3) {
      System.out.println("\nYou exit the Green Fox.");
      VarDormin.varDormin();
    }
    else {
      VarDormin.inn();
    }
  }

  public static void slayDragon() {
    dragonSlain = true;
  }

  public static boolean dragonSlain() {
    return dragonSlain;
  }
}