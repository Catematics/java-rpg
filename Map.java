import java.util.Scanner;
import java.util.Random;

public class Map {
  private static Scanner scan = new Scanner(System.in);
  private static Random rand = new Random();
  private static String move;
  private static int x = 4;
  private static int y = 3;
  private static int top = 7;
  private static String[] map =
  {"^^^^^^^^^^        ≈≈≈≈≈≈≈  ^^^^",
   "^^^^^  ♛     ^^^    ≈≈≈     ^^^",
   "^^^         ^^^^       ^^     ^",
   "^      ≈≈≈    ^^^     ^♝^^     ",
   "     ≈≈≈≈≈≈         ^^^^^^     ",
   "      ≈≈≈≈            ^^       ",
   "               ^^^             ",
   "    ♝        ^^^^^^         ≈≈≈",
   "               ^^^^     ♜ ≈≈≈≈≈",
   "^^^^^         ^^^      ≈≈≈≈≈≈≈≈",
   "^^^^^^^              ≈≈≈≈≈≈≈≈≈≈"};

  public static void move() {
    boolean running = true;

    System.out.println("\nMove with W, A, S, and D.");

    while(running) {
      Map.printMap();
      move = scan.nextLine();
      Map.getMove(move);

      if(x == 4 && y == 3) {
        System.out.println("\nYou arrive at your home town of Tallimore.");
        Tallimore.tallimore();
        break;
      }
      else if(x == 7 && y == 9) {
        System.out.println("\nYou arrive at the Northern Kingdom of Fazoria.");
        Fazoria.fazoria();
        break;
      }
      else if(x == 23 && y == 7) {
        System.out.println("\nYou arrive at the mountain town of Var Dormin.");
        VarDormin.varDormin();
        break;
      }
      else if(x == 24 && y == 2) {
        System.out.println("\nYou arrive at the port city of Yandelport.");
        Yandelport.yandelport();
        break;
      }
      else if(map[top].substring(x, (x + 1)).equals("^")) {
        Battle.randEncounter();
      }
      else if(map[top].substring(x, (x + 1)).equals("≈")) {
        System.out.println("\nYou can't swim!");
        Character.loseLife(3);
        if(Character.getLife() < 1) {
          Character.gameOver("They drowned.");
        }
      }
      else {
        int randEncounter = rand.nextInt(8);
        if(randEncounter == 0) {
          Battle.randEncounter();
        }
      }
    }
  }

  public static void getMove(String move) {
    if(move.equals("w") || move.equals("W")) {
      top--;
      y++;
    }
    else if(move.equals("a") || move.equals("A")) {
      x--;
    }
    else if(move.equals("s") || move.equals("S")) {
      top++;
      y--;
    }
    else if(move.equals("d") || move.equals("D")) {
      x++;
    }

    if(x > map[0].length() - 1) {
      x = map[0].length() - 1;
    }
    if(x < 0) {
      x = 0;
    }
    if(top > map.length - 1) {
      top = map.length - 1;
      y = 0;
    }
    if(y > map.length - 1) {
      top = 0;
      y = map.length - 1;
    }
  }

  public static void printMap() {
    System.out.println("===============================");

    for(int a = 0; a < top; a++) {
      System.out.println(map[a]);
    }

    System.out.print(map[top].substring(0, x));

    System.out.print(Character.printSymbol());
    
    System.out.print(map[top].substring(x + 1, map[top].length()));
    System.out.println();

    for(int d = top; d < top + y; d++) {
      System.out.println(map[d + 1]);
    }

    System.out.println("===============================");
    System.out.println("Life: " + Character.getLife() + "/" + Character.getMaxLife() + "\t\tX: " + x + "\tY: " + y);
  }
}