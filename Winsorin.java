import java.util.Scanner;

public class Winsorin {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;

  public static void winsorin() {
    System.out.println("\n\t1. Look around\n\t2. Visit the shop\n\t3. Visit the inn\n\t4. Board a boat\n\t5. Leave Winsorin");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou take a look around Winsorin. It is similar to Yandelport, only much smaller, but the people residing here are much friendlier. There is a statue of an old king in the town square.");
      Winsorin.winsorin();
    }
    else if(choice == 2) {
      System.out.println("\nYou go to Winsorin's eccentric shop.");
      Shop.winsorinShop();
    }
    else if(choice == 3) {
      System.out.println("\nYou visit the Blue Crab, an inn right by the seashore.");
      Winsorin.inn();
    }
    else if(choice == 4) {
      System.out.println("\nYou board your boat and head back to Yandelport.");
      Yandelport.yandelport();
    }
    else if(choice == 5) {
      System.out.println("\nYou leave the town of Winsorin.");
      Map2.move();
    }
    else {
      Winsorin.winsorin();
    }
  }

  public static void inn() {
    System.out.println("\n\t1. Check status\n\t2. Rest - 5 Gold\n\t3. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      Character.status();
      Winsorin.inn();
    }
    else if(choice == 2) {
      if(Shop.getGold() >= 5) {
        Shop.changeGold(-5);
        Character.heal();
        System.out.println("\nYou rent a room and rest there for a while, restoring your life.");
      }
      else {
        System.out.println("\nNot enough gold!");
      }
      Winsorin.inn();
    }
    else if(choice == 3) {
      System.out.println("\nYou exit the Blue Crab.");
      Winsorin.winsorin();
    }
    else {
      Winsorin.inn();
    }
  }
}