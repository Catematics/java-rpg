public class Zariadon {
  public static void zariadon() {
    System.out.println("\nYou cautiously explore the ancient ruins of Zariadon. All of a sudden, you hear a voice coming from behind you. You turn to see a horrific beast with the face of a human. To your surprise, the beast is able to speak.");
    System.out.println("\n\"I am Thoxallimus, once a renowned king, now an abomination. Now you must perish, just like the hundreds of others who came here.\"");
    Battle.thoxallimusBattle();
  }
}