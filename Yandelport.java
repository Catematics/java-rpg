import java.util.Scanner;

public class Yandelport{
  private static Scanner scan = new Scanner(System.in);
  private static int choice;
  private static boolean hasBoat = false;

  public static void yandelport() {
    System.out.println("\n\t1. Look around\n\t2. Visit the marketplace\n\t3. Visit the inn\n\t4. Board a boat\n\t5. Leave Yandelport");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou take a look around Yandelport. Half of it is a residential city, and the other half is an industrial port. Dozens of ships are boarded at the docks, although many of them seem to be transporting goods.");
      Yandelport.yandelport();
    }
    else if(choice == 2) {
      System.out.println("\nYou visit Yandelport's large marketplace on the docks.");
      Shop.yandelportShop();
    }
    else if(choice == 3) {
      System.out.println("\nYou visit the Golden Fish, an inn located by the massive port.");
      Yandelport.inn();
    }
    else if(choice == 4) {
      if(!hasBoat) {
        System.out.println("\nYou go to the port, but you can't find any boats that are open to the general public. You finally come across a hooded man who says that he can rent you his boat for the price of 5000 gold.");
        Yandelport.port();
      }
      else {
        System.out.println("\nYou set sail towards the continent across the sea. You arrive in the modest port town of Winsorin.");
        Winsorin.winsorin();
      }
    }
    else if(choice == 5) {
      System.out.println("\nYou leave the port city of Yandelport.");
      Map.move();
    }
    else {
      Yandelport.yandelport();
    }
  }

  public static void inn() {
    System.out.println("\n\t1. Talk to the innkeeper\n\t2. Check status\n\t3. Rest - 5 Gold\n\t4. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou talk to a tall man who you assume to be the inkeeper. He sits there stoically, but nods his head.");
      Yandelport.inn();
    }
    else if(choice == 2) {
      Character.status();
      Yandelport.inn();
    }
    else if(choice == 3) {
      if(Shop.getGold() >= 5) {
        Shop.changeGold(-5);
        Character.heal();
        System.out.println("\nYou rent a room and rest there for a while, restoring your life.");
      }
      else {
        System.out.println("\nNot enough gold!");
      }
      Yandelport.inn();
    }
    else if(choice == 4) {
      System.out.println("\nYou exit the Golden Fish.");
      Yandelport.yandelport();
    }
    else {
      Yandelport.inn();
    }
  }

  public static void port() {
    System.out.println("\n\t1. Accept\n\t2. Decline");
    choice = scan.nextInt();
    if(choice == 1) {
      if(Shop.getGold() >= 5000) {
        Shop.changeGold(-5000);
        System.out.println("\nYou accept the offer. You have obtained a boat!");
        hasBoat = true;
        Yandelport.yandelport();
      }
      else {
        System.out.println("\nNot enough gold!");
        Yandelport.port();
      }
    }
    else if(choice == 2) {
      System.out.println("\nYou decline the offer.");
      Yandelport.yandelport();
    }
    else {
      Yandelport.port();
    }
  }
}