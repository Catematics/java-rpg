import java.util.Scanner;
import java.util.Random;

public class Map2 {
  private static Scanner scan = new Scanner(System.in);
  private static Random rand = new Random();
  private static String move;
  private static int x = 8;
  private static int y = 4;
  private static int top = 6;
  private static String[] map =
  {"   ^^^^^^^   ✠  ^^^^^^^^^^^^^^^",
   " ♜   ^^^^^^^  ^^^^^ ^^^^^  ^^^^",
   "^       ^^^^^^^^^     ^^   ♛  ^",
   "^^^        ^^^^^             ≈≈",
   "  ^^                       ≈≈≈≈",
   "≈≈≈ ^         ^^            ≈≈≈",
   "≈≈≈≈≈≈≈ ♝    ^^^    ^^   ^^    ",
   "≈≈≈≈≈≈≈≈≈≈         ^^^^^  ^^^^ ",
   "≈≈≈≈≈≈≈≈≈≈≈≈≈       ^^^^^^^^♝^^",
   "≈≈≈≈≈≈≈≈≈≈≈≈≈≈    ^^ ^^^^^≈≈≈≈^",
   "≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈       ^^^^≈≈≈≈"};

  public static void move() {
    boolean running = true;

    System.out.println("\nMove with W, A, S, and D.");

    while(running) {
      Map2.printMap();
      move = scan.nextLine();
      Map2.getMove(move);

      if(x == 8 && y == 4) {
        System.out.println("\nYou arrive at the modest port town of Winsorin.");
        Winsorin.winsorin();
        break;
      }
      else if(x == 1 && y == 9) {
        System.out.println("\nYou arrive at the military fortress of Hanawyn.");
        Hanawyn.hanawyn();
        break;
      }
      else if(x == 28 && y == 2) {
        System.out.println("\nYou arrive at the forested town of Gladehollow.");
        Gladehollow.gladehollow();
        break;
      }
      else if(x == 27 && y == 8) {
        System.out.println("\nYou arrive at the Imperial City of Kalldirid.");
        Kalldirid.kalldirid();
        break;
      }
      else if(x == 13 && y == 10) {
        System.out.println("\nYou arrive at an ancient, abandoned tower.");
        OldTower.oldTower();
      }
      else if(map[top].substring(x, (x + 1)).equals("^")) {
        Battle.randEncounter();
      }
      else if(map[top].substring(x, (x + 1)).equals("≈")) {
        System.out.println("\nYou can't swim!");
        Character.loseLife(3);
        if(Character.getLife() < 1) {
          Character.gameOver("\nThey drowned.");
        }
      }
      else {
        int randEncounter = rand.nextInt(8);
        if(randEncounter == 0) {
          Battle.randEncounter();
        }
      }
    }
  }

  public static void getMove(String move) {
    if(move.equals("w") || move.equals("W")) {
      top--;
      y++;
    }
    else if(move.equals("a") || move.equals("A")) {
      x--;
    }
    else if(move.equals("s") || move.equals("S")) {
      top++;
      y--;
    }
    else if(move.equals("d") || move.equals("D")) {
      x++;
    }

    if(x > map[0].length() - 1) {
      x = map[0].length() - 1;
    }
    if(x < 0) {
      x = 0;
    }
    if(top > map.length - 1) {
      top = map.length - 1;
      y = 0;
    }
    if(y > map.length - 1) {
      top = 0;
      y = map.length - 1;
    }
  }

  public static void printMap() {
    System.out.println("===============================");

    for(int a = 0; a < top; a++) {
      System.out.println(map[a]);
    }

    System.out.print(map[top].substring(0, x));

    System.out.print(Character.printSymbol());
    
    System.out.print(map[top].substring(x + 1, map[top].length()));
    System.out.println();

    for(int d = top; d < top + y; d++) {
      System.out.println(map[d + 1]);
    }

    System.out.println("===============================");
    System.out.println("Life: " + Character.getLife() + "/" + Character.getMaxLife() + "\t\tX: " + x + "\tY: " + y);
  }
}