import java.util.Scanner;

public class Gladehollow {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;

  public static void gladehollow() {
    System.out.println("\n\t1. Look around\n\t2. Visit the shop\n\t3. Visit the inn\n\t4. Explore the forest\n\t5. Leave Gladehollow");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou look around Gladehollow. The buildings and the environment are integrated together. All of the residents are quiet, but relaxed and friendly for the most part. A dense forest borders the town of all sides, and a lake can be found to the south.");
      Gladehollow.gladehollow();
    }
    else if(choice == 2) {
      Shop.gladehollowShop();
    }
    else if(choice == 3) {
      System.out.println("\nYou visit the Gray Wolf, an inn carved into the trunk of a giant tree.");
      Gladehollow.inn();
    }
    else if(choice == 4) {
      System.out.println("\nYou enter the vast forest located by Gladehollow.");
      Gladehollow.forest();
    }
    else if(choice == 5) {
      System.out.println("\nYou leave the forest-town of Gladehollow.");
      Map2.move();
    }
    else {
      Gladehollow.gladehollow();
    }
  }

  public static void inn() {
    System.out.println("\n\t1. Check status\n\t2. Rest - 5 Gold\n\t3. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      Character.status();
      Gladehollow.inn();
    }
    else if(choice == 2) {
      if(Shop.getGold() >= 5) {
        Shop.changeGold(-5);
        Character.heal();
        System.out.println("\nYou rent a room and rest there for a while, restoring your life.");
      }
      else {
        System.out.println("\nNot enough gold!");
      }
      Gladehollow.inn();
    }
    else if(choice == 3) {
      System.out.println("\nYou exit the Gray Wolf.");
      Gladehollow.gladehollow();
    }
    else {
      Gladehollow.inn();
    }
  }

  public static void forest() {
    if(!Kalldirid.lupineKnightSlain()) {
      System.out.println("\nYou hear the menacing howl of a wolf.");
    }
    System.out.println("\n\t1. Look around\n\t2. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      if(!Kalldirid.lupineKnightSlain()) {
        System.out.println("\nAs you look around the forest, you encounter the Lupine Knight.");
        Battle.lupineKnightBattle();
      }
      else {
        System.out.println("\nYou walk around the forest. You see a variety of beautiful trees and flowers, along with a bunch of interesting animals. You walk back to Gladehollow before you get too lost.");
        Gladehollow.gladehollow();
      }
    }
    else if(choice == 2) {
      System.out.println("\nYou exit the forest.");
      Gladehollow.gladehollow();
    }
    else {
      Gladehollow.forest();
    }
  }
}