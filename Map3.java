import java.util.Scanner;
import java.util.Random;

public class Map3 {
  private static Scanner scan = new Scanner(System.in);
  private static Random rand = new Random();
  private static String move;
  private static int x = 15;
  private static int y = 0;
  private static int top = 10;
  private static String[] map =
  {"^^^^^^ ^^^^^^^^^^^^^^^^ ^^^^^^^",
   "^^^^^^^^ ^^^^^^ ^^ ^^^^^^^^^ ^^",
   "^^^^^^ ^^^^ ^     ^ ^^^^^^^^^^^",
   "^^^^^^^^^^ ^ ^  ✠    ^^ ^^^^^^ ",
   "^^^ ^^^^  ^^^      ^  ^^^^^^ ^^",
   "^^^^^^^^^^^^ ^  ^    ^   ^^^^^^",
   "^^^^ ^ ^^^^ ^^^  ^   ^^^^^ ^^^^",
   "^^^^^^^^^^^^   ^^   ^^ ^^^^^^^ ",
   "^^^^^^^^ ^^^^^^  ^   ^^^^^ ^^^^",
   "^^ ^^^^^^^^^^^ ^  ^^  ^^^^^^^^^",
   "^^^^^^^^^^ ^^     ^  ^^ ^^^^^^^"};

  public static void move() {
    boolean running = true;

    System.out.println("\nMove with W, A, S, and D.");

    while(running) {
      Map3.printMap();
      move = scan.nextLine();
      Map3.getMove(move);

      if(x == 16 && y == 7) {
        System.out.println("\nYou arrive at the the once-great city of Zariadon, now a heap of forgotten ruins.");
        Zariadon.zariadon();
      }
      else if(map[top].substring(x, (x + 1)).equals("^")) {
        Battle.advancedEncounter();
      }
      else {
        int randEncounter = rand.nextInt(5);
        if(randEncounter == 0) {
          Battle.advancedEncounter();
        }
      }
    }
  }

  public static void getMove(String move) {
    if(move.equals("w") || move.equals("W")) {
      top--;
      y++;
    }
    else if(move.equals("a") || move.equals("A")) {
      x--;
    }
    else if(move.equals("s") || move.equals("S")) {
      top++;
      y--;
    }
    else if(move.equals("d") || move.equals("D")) {
      x++;
    }

    if(x > map[0].length() - 1) {
      x = map[0].length() - 1;
    }
    if(x < 0) {
      x = 0;
    }
    if(top > map.length - 1) {
      top = map.length - 1;
      y = 0;
    }
    if(y > map.length - 1) {
      top = 0;
      y = map.length - 1;
    }
  }

  public static void printMap() {
    System.out.println("===============================");

    for(int a = 0; a < top; a++) {
      System.out.println(map[a]);
    }

    System.out.print(map[top].substring(0, x));

    System.out.print(Character.printSymbol());
    
    System.out.print(map[top].substring(x + 1, map[top].length()));
    System.out.println();

    for(int d = top; d < top + y; d++) {
      System.out.println(map[d + 1]);
    }

    System.out.println("===============================");
    System.out.println("Life: " + Character.getLife() + "/" + Character.getMaxLife() + "\t\tX: " + x + "\tY: " + y);
  }
}