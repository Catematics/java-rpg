/**
 * A simple role-playing game programmed in Java.
 *
 * The user can create a character, explore
 * the world, and battle against enemies.
 *
 * @author  Drake Mayes
 */

class Main {
  public static void main(String[] args) {
    Tallimore.beginning();
  }
}