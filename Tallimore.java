import java.util.Scanner;

public class Tallimore {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;

  public static void beginning() {
    System.out.println("\nYour eyes flutter open and you roll out of your cloth bed. Sunlight is shining into your room. Your mind is still foggy, so you must remind yourself what your name is.");
    System.out.print("\nEnter your name: ");
    String name = scan.nextLine();
    Character.setName(name);
    System.out.println("\nAh, that's right. Your name is " + Character.getName() + ".");
    Character.setRole();
    System.out.println("\nYou yawn, then walk outside. It's a beautiful day in your home town of Tallimore. People are already outside mingling and the birds are singing.");
    Tallimore.tallimore();
  }

  public static void tallimore() {
    System.out.println("\n\t1. Look around\n\t2. Visit the shop\n\t3. Go to your house\n\t4. Leave Tallimore");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYou look around, talking to a few old friends along the way. Nothing too interesting seems to be going on.");
      Tallimore.tallimore();
    }
    else if(choice == 2) {
      System.out.println("\nYou head to the shop located at the heart of Tallimore.");
      Shop.tallimoreShop();
    }
    else if(choice == 3) {
      System.out.println("\nYou return to your own humble abode.");
      Tallimore.yourHouse();
    }
    else if(choice == 4) {
      System.out.println("\nYou say goodbye to your friends and leave the town of Tallimore.");
      Map.move();
    }
    else {
      Tallimore.tallimore();
    }
  }

  public static void yourHouse() {
    System.out.println("\n\t1. Look around\n\t2. Check status\n\t3. Rest\n\t4. Leave");
    choice = scan.nextInt();
    if(choice == 1) {
      System.out.println("\nYour house is just the way you left it, your bed in one corner and a table in the other.");
      if(Shop.getGold() == 0) {
        System.out.println("\nYou find some spare gold while searching around your house.");
        Shop.changeGold(2);
      }
      Tallimore.yourHouse();
    }
    else if(choice == 2) {
      Character.status();
      Tallimore.yourHouse();
    }
    else if(choice == 3) {
      System.out.println("\nYou close your eyes for a bit. Your life has been restored.");
      Character.heal();
      Tallimore.yourHouse();
    }
    else if(choice == 4) {
      System.out.println("You exit your house.");
      Tallimore.tallimore();
    }
    else {
      Tallimore.yourHouse();
    }
  }
}