import java.util.Scanner;

public class OldTower {
  private static Scanner scan = new Scanner(System.in);
  private static int choice;

  public static void oldTower() {
    if(!Kalldirid.leonineKnightSlain()) {
      System.out.println("\nYou feel the prescence of something watching you. Enter the tower?");
      System.out.println("\n\t1. Yes\n\t2. No");
      choice = scan.nextInt();
      if(choice == 1) {
        System.out.println("\nYou enter the old tower, and you find the Leonine Knight waiting for you.");
        Battle.leonineKnightBattle();
      }
      else if(choice == 2) {
        System.out.println("\nYou skedaddle out of the old tower.");
        Map2.move();
      }
      else {
        OldTower.oldTower();
      }
    }
    else {
      System.out.println("\nYou examine the tower again, but decide there's nothing to do here.");
      Map2.move();
    }
  }
}